<?php 

class Controller 
{
    public function __construct()
    {
        $this->access("login_required");
    }

    public function view($view, $data = [])
    {
        require_once '../app/views/' . $view . '.php';
    }

    public function model($name){
        require_once "../app/models/".$name.'.php';
        return new $name();
    }

    public function acceptMethod(string $httpMethod){
        if($httpMethod !== $_SERVER['REQUEST_METHOD'])
        {
            http_response_code(400);
            die;
        }
    }

    public function access(string $access)
    {
        switch($access){
            case "guest":
                if(isset($_SESSION['user_login'])){
                    return redirect("/");
                }
                break; 
            case "login_required":
                if(! isset($_SESSION['user_login'])){
                    return redirect("credential/login");
                }
                break;
        }
    }
}