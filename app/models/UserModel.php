<?php

class UserModel extends Model 
{
    public string $table = 'User';
    
    public array $data = [];

    public function cobaLogin(string $email, string $password)
    {
        $sql = "SELECT * FROM user WHERE email = :email";
        $passwordValidKah = false;

        $this->db->query($sql);
        $this->db->bind("email",$email);

        $user = $this->db->single();

        if($user){
          $passwordValidKah = Hash::check( $password, $user['password'] );
        }
        
        if ($passwordValidKah)
        {
          $_SESSION['user_login'] = [
            "nama" => $user['nama'],
            "email" => $user['email'],
            "user_id" => $user['user_id'],
            "posisi" => $user['posisi']
          ];
          return redirect("/");
        }
        
        return redirect("credential/login",['fail' => "Email Atau Password Salah"]);
    }
}   