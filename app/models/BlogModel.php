<?php

class BlogModel extends Model {
    public string $table = "blog";

    public string $commonSql = "SELECT blog_id, title, description, user_id, nama AS author FROM blog INNER JOIN user ON fk_user_id = user_id";
    

    public function all()
    {
        $this->db->query($this->commonSql);
        return $this->db->resultSet();
    }

    public function get(int $id)
    {
        $this->db->query("{$this->commonSql} WHERE blog_id = :blog_id");
        $this->db->bind(":blog_id", $id);
        return $this->db->single();
    }

}   