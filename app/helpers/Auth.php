<?php

//sebuah kelas yang berhubungan dengan orang login
class Auth
{
    PUBLIC CONST SESSION_KEY = "user_login",
                  EMAIL = "email",
                  NAMA = "nama",
                  POSISI = 'posisi',
                  PRIMARY_KEY = 'user_id';
    
    public static function login(array $data)
    {
        $_SESSION[self::SESSION_KEY] = $data;
    }

    public static function id()
    {
        return $_SESSION[self::SESSION_KEY][self::PRIMARY_KEY] ?? null;
    }

    public static function user()
    {
        return $_SESSION[self::SESSION_KEY] ?? null;
    }

    public static function email()
    {
        return $_SESSION[self::SESSION_KEY][self::EMAIL] ?? null;
    }

    public static function nama()
    {
        return $_SESSION[self::SESSION_KEY][self::NAMA] ?? null;
    }

    public static function logout()
    {
        unset($_SESSION[self::SESSION_KEY]);
        session_unset();
        session_destroy();
    }

    public static function isAdmin()
    {
        return $_SESSION[self::SESSION_KEY][self::POSISI] === "admin";
    }
}