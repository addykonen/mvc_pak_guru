<?php

class Credential extends Controller
{
    private UserModel $userModel;

    public function __construct()
    {
        $this->userModel = $this->model("UserModel");
    }

    public function register()
    {
        $this->access("guest");
        $data['title'] = "Register";
        
        
        return $this->view("user/register", $data);
    }

    public function simpan_register()
    {
        $this->access("guest");
        $password = Hash::make($_POST['password']);
       
        if ($_POST['password'] !== $_POST['confirm_password'])
        {
            return redirect("credential/register",['fail' => 'password dan konfirmasi password harus sama!']);
        }

        Validator::check([
            "nama" => ['min' => 4, 'max' => '80'],
            "email" => ['min' => 4, 'max' => '255'],
            "password" => ['min' => 4, 'max' => '255'],
            "no_telpon" => ['min' => 10, 'max' => 30]
        ])
        ->ifHasErrorThrowTo("credential/register");

        try {
            $this->userModel->create([
                "nama" => $_POST['nama'],
                "password" => $password,
                "no_telpon" => $_POST['no_telpon'],
                "email" => $_POST['email']
            ]);
        }
        
        catch(Exception $e){
            return redirect("/credential/regiser", ['fail' => 'email sudah terdaftar atau terjadi kesalahan sistem!']);
        }

        return redirect('/credential/login', ['success' => 'register berhasil silahkan login']);
    }

    public function logout()
    {
        $this->access("login_required");
        Auth::logout();
        return redirect('credential/login');
    }

    public function login()
    {
        $this->access("guest");
        $data['title'] = "Login";
        return $this->view("user/login", $data);
    }

    public function kontrol_login()
    {
        $this->access('guest');
        $this->userModel->cobaLogin($_POST['email'], $_POST['password']);
    }

    public function echoTest($str)
    {
        echo $str;
    }

    
}