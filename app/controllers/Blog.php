<?php 
class Blog extends Controller {

    private BlogModel $blogModel;

    public function __construct()
    {
        parent::__construct();
        $this->blogModel = $this->model("BlogModel");
    }

    public function index()
    {
        $blogModel = $this->model("BlogModel");
        $data['title'] = "blog - BlogMusic";
        $data['semua_blog'] = $blogModel->all();
        
        $this->view('templates/header',$data);  
        $this->view("blog/index",$data);
        $this->view('templates/footer');
    }

    public function create()
    {
        $data['title'] = "Register Music";
        $this->view("templates/header", $data);
        $this->view("blog/create",$data);
        $this->view("templates/footer");
    }

    public function edit(int $id)
    {
        $blogModel = $this->model("BlogModel");
        $arr = $blogModel->get($id);
        $data['title'] = "Edit Blog " . $arr['title'];
        $data['single_blog'] = $arr;
      

        $this->view("templates/header", $data);
        $this->view("blog/edit",$data);
        $this->view("templates/footer");
    }

    //POST METHOD
    public function store()
    {
        $this->acceptMethod("POST");

        Validator::check([
            'title' => ["min" => 10, "max" => 255],
            'description' => ['min' => 20, 'max' => 2000]
        ])->ifHasErrorThrowTo("blog/create");
      
        $this->blogModel->create([
            "title" => $_POST['title'],
            "description" => $_POST['description'],
            "fk_user_id" => Auth::id()
        ]);

        return redirect("blog/index", ["success" => "Berhasil Membuat Blog!"]);
    }
    
    public function update(int $id)
    {   
        $this->acceptMethod("POST");
        Validator::check([
            'title' => ['min' => 5, 'max' => 255],
            'description' => ['min' => 20, 'max' => 2000]
        ])->ifHasErrorThrowTo('blog/edit/'.$id);
        
        try {
            $blogModel = $this->model("BlogModel");
        } catch(Exception $error){
            return redirect("blog/edit/$id",['fail' => 'gagal update']);
        }
        
        $blogModel->update($id, $_POST);
        return redirect("blog/edit/$id", ['success' => 'berhasil edit blog!']);
    }

    public function hapus(int $blog_id)
    {
        $this->acceptMethod("POST");    
        $this->blogModel->delete($blog_id);
        return redirect("blog/index",['fail' => 'gagal menghapus']);
    }

    public function show(int $blog_id)
    {
        $data['blog'] = $this->blogModel->get($blog_id);

        if($data['blog']){
            $data['title'] = "Blog " . $data['blog']['author'] . " | ". $data['blog']['title'];
        }

        else {
            http_response_code(404);
            echo "404";
            die;
        }

        $this->view("templates/header", $data);
        $this->view("blog/show",$data);
        $this->view("templates/footer");
    }
}