<?php

require_once "../app/core/Database.php";
require_once "../app/core/Model.php";
require_once "../app/config/config.php";
require_once "../app/helpers/functions.php";
require_once "../app/helpers/Validator.php";
require_once "../app/helpers/Hash.php";
require_once "../app/helpers/Auth.php";
require_once "../app/core/App.php";
require_once "../app/core/Controller.php";