<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $data['title']?></title>
    <link rel="stylesheet" href="<?= url("css/sb-admin-2.min.css")?>">
    <script src="<?= url('js/jquery.js')?>"></script>
</head>
<body class="bg-gradient-dark">
    <nav class="navbar shadow-lg navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="<?= BASE_URL?>">BlogMusic<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-music-note" viewBox="0 0 16 16">
        <path d="M9 13c0 1.105-1.12 2-2.5 2S4 14.105 4 13s1.12-2 2.5-2 2.5.895 2.5 2z"/>
        <path fill-rule="evenodd" d="M9 3v10H8V3h1z"/>
        <path d="M8 2.82a1 1 0 0 1 .804-.98l3-.6A1 1 0 0 1 13 2.22V4L8 5V2.82z"/>
        </svg>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
     </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto" >
    
      <li class="nav-link active">
        <a href="<?= url('/')?>" class="nav-link">
          Home
        </a>
      </li>
      <li class="nav-link active">
        <a href="<?= url('blog/create')?>" class="nav-link">
          Buat Blog
        </a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <a href=<?= url('credential/logout')?> class="text-white nav-link">Logout</a>
    </form>
  </div>
</nav>
<div class="container py-3 shadow-sm">