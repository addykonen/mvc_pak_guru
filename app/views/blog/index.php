<?php $i = 0;?>
<?php foreach($data['semua_blog'] as $blog):?>
<div class="card shadow mb-4 text-white bg-gradient-dark">                          
    <div class="card-header bg-gradient-secondary py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-light"><?= $blog['author']?></h6>
        <?php if($blog['user_id'] === Auth::id() || Auth::isAdmin()):?>
        <div class="dropdown text-light">
            <a class="dropdown-toggle text-light" hre   f="#" role="button" id="dropdownMenuLink_<?= $i?>"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                aria-labelledby="dropdownMenuLink_<?= $i?>">
                <div class="dropdown-header">Aksi:</div>
                <a class="dropdown-item" href="<?= url("blog/edit/".$blog['blog_id'])?>">Edit</a>
                <form method="POST" action="<?= url("blog/hapus/".$blog['blog_id'])?>" class="d-inline">
                    <button class="dropdown-item">Hapus</button>
                </form>
            </div>
        </div>
        <?php endif;?>
    </div>
    <!-- Card Body -->
    <div class="card-body">
       <p><?= $blog['title']?></p>
       <p><?= $blog['description']?></p>
       <a href=<?= url("blog/show/{$blog['blog_id']}")?> class="text-success text-decoration-none font-weight-bold">Selengkapnya</a>
    </div>
</div>
<?php endforeach;?>