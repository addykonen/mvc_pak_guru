<div class="container bg-dark shadow-lg border  text-white p-5 mt-5" style="border-radius: 1rem;">
    <h1 class="mb-2">Buat Blog Disini</h1>
    <form class="row" method="POST" action="<?= BASE_URL?>/blog/store">
        <div class="col-md-12 my-2">
            <label for="name">Title</label>
            <input required type="text" id="name" class="form-control <?php if($_SESSION['title']):?> is-invalid <?php endif?>" name="title">
            <span class="text-warning fw-bold"><?= error('title')?></span>
        </div>
        <div class="col-md-12 my-2">
            <label for="name">Description</label>
            <textarea required name="description" class="form-control <?php if($_SESSION['title']):?> is-invalid <?php endif;?>" id="" cols="30" rows="10" name="description"></textarea>
            <span class="text-warning"><?= error('description')?></span>
        </div>
        <button class="btn-success btn m-3">Buat Blog Baru</button>
        <a href="<?= url('blog')?>" class="btn btn-secondary my-3">Kembali</a>
    </form>
</div>