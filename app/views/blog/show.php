<div class="card shadow mb-4 text-white bg-gradient-dark">                          
    <div class="card-header bg-gradient-secondary py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-light"><?= $data['blog']['author']?></h6>
    </div>
    <!-- Card Body --> 
    <div class="card-body">
       <p><?= $data['blog']['title']?></p>
       <p><?= $data['blog']['description']?></p>
       <a href="<?= url('/')?>" class="text-success text-decoration-none font-weight-bold">Kembali</a>
    </div>
</div>