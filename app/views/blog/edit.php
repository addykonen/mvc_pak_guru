<div class="container bg-dark shadow-lg border  text-white p-5 mt-5" style="border-radius: 1rem;">
    <h1 class="mb-2">Edit Blog Disini</h1>
    <form method="POST" action="<?= url("blog/update/{$data['single_blog']['blog_id']}")?>" class="row">
        <div class="col-md-12 my-2">
            <label for="title">Title</label>
            <input required type="text" id="title" name="title" class="form-control" value="<?= $data['single_blog']['title']?>">
            <span class="text-warning"><?= error('title')?></span>
        </div>
        <div class="col-md-12 my-2">
            <label for="description">Description</label>
            <textarea required name="description" class="form-control" id="description" cols="30" rows="10"><?=$data['single_blog']['description']?></textarea>
            <span class="text-warning"><?= error('description')?></span>
        </div>
        <input type="submit" class="btn-success btn mt-2 mx-3" value="Update Blog" />
        <a href="<?= url('blog')?>" class="btn btn-secondary mt-2 ">Kembali</a>
    </form>
</div>