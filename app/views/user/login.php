<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= url("css/sb-admin-2.min.css")?>" rel="stylesheet">
    
</head>

<body class="bg-gradient-dark">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-md-8">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row bg-dark text-white justify-content-center">
                          
                            <div class="col-lg-11">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 mb-2 font-weight-bold text-white">Login BlogMusic</h1>
                                    </div>
                                    <form class="user" method="POST" action="<?= url('credential/kontrol_login')?>">
                                        
                                        <div class="form-group ">
                                            <label for="email">Email</label>
                                            <input name="email" type="email" class="bg-dark text-white form-control form-control-user py-4"
                                                id="email" placeholder="Masukan Email...">
                                        </div>
                                        
                                        <div class="form-group ">
                                            <label for="password">Password</label>
                                            <input name="password" type="password" class="bg-dark text-white form-control form-control-user py-4"
                                                id="password" placeholder="Masukan Password....">
                                        </div>
                                       

                                        <button class="
                                        btn btn-secondary mt-4 btn-user btn-block">
                                               Login
                                        </button>
                                        <div class="mt-1 text-center">
                                            Tidak Punya Akun?<a href=<?= url('credential/register')?> class="text-success font-weight-bold btn">Register Disini</a>
                                        </div>
                                    </form>
                          
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>